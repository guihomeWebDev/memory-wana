<?php 
 //create sqli database connection 
    $db = new mysqli('localhost', 'root', '', 'sqli');
    if($db->connect_errno > 0){
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    //create sql statement
    $sql = "SELECT * FROM users";
    //execute sql statement
    if(!$result = $db->query($sql)){
        die('There was an error running the query [' . $db->error . ']');
    }
    //create array to store results
    $users = array();
    //loop through results and add to array
    while($row = $result->fetch_assoc()){
        $users[] = $row;
    }
    //close database connection
    $db->close();
?>